/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//start server first! then client
package server;

/**
 *
 * @author Kerstin, Anna, Dahn and Hesha
 */
// A Java program for a Server
import java.net.*;
import java.io.*;
import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.v26.message.ADT_A01;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.parser.Parser;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server
{
    //initialize socket and input stream
    private Socket          socket   = null;
    private ServerSocket    server   = null;
    private DataInputStream in       =  null;
    public static  ADT_A01 adt;
    
    public static String first = "";
    public static String last = "";
    public static String birthdate = "";
    public static String height = "";
    public static String weight = "";
    public static Date   datetimeBirth;
    
    public String writeInput (String variable) {
        try
            {
               variable = in.readUTF();
               System.out.println(variable);
            }
            catch(IOException i)
            {
               System.out.println(i);
            }
        
        return variable;
    }

    // constructor with port
    public Server(int port) throws ParseException
    {
       
        // starts server and waits for a connection
        try
        {
            server = new ServerSocket(port);
            System.out.println("Server started");

            System.out.println("Waiting for a client ...");

            socket = server.accept();
            System.out.println("Client accepted");

            // takes input from the client socket
            in = new DataInputStream(
                new BufferedInputStream(socket.getInputStream()));

            // reads message from client until "Over" is sent
            first = writeInput(first);
            last = writeInput(last);
            birthdate = writeInput(birthdate);
            height = writeInput(height);
            weight= writeInput(weight);
            
            datetimeBirth =new SimpleDateFormat("dd.MM.yyyy").parse(birthdate);  


            System.out.println("My name is "+first+ "" + last+ ", I am "+ birthdate + " years old and I am " + height+" cm talls and I weight " + weight);
            System.out.println("Closing connection");

            // close connection
            socket.close();
            in.close();
        }
        catch(IOException i)
        {
            System.out.println(i);
        }
    }

    public static void main(String[] args) throws Exception
    {
        Server server = new Server(5000);
        
        adt = new ADT_A01();
        adt.initQuickstart("ADT", "A01", "P");
        
          // Populate the MSH Segment
          MSH mshSegment = adt.getMSH();
          mshSegment.getSendingApplication().getNamespaceID().setValue("PatientRecord");
          mshSegment.getSequenceNumber().setValue("123");
          

          // Populate the PID Segment
          PID pid = adt.getPID(); 
          pid.getPatientName(0).getFamilyName().getSurname().setValue(last);
          pid.getPatientName(0).getGivenName().setValue(first);
          pid.getDateTimeOfBirth().setValue(datetimeBirth);
          pid.getPatientIdentifierList(0).getIDNumber().setValue("123456");
          
          HapiContext context = new DefaultHapiContext();
          Parser pipeParser = context.getPipeParser();
          System.out.println("Message was constructed succesfully.." + "\n");
          System.out.println(pipeParser.encode(adt));
          
          //String encodedMessage = pipeParser.encode(adt);
          /*System.out.println("Printing ER7 Encoded Message:");
          System.out.println(encodedMessage);*/
          
          
          
          /*
          parser = context.getXMLParser();
          encodedMessage = parser.encode(adt);
          System.out.println("Printing XML Encoded Message:");
          System.out.println(encodedMessage);*/
    }
}