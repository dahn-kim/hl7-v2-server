/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package client;

/**
 *
 * @author Kerstin, Anna, Dahn and Hesha
 */
import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client {
// initialize socket and input output streams
    private Socket socket            = null;
    private DataInputStream  input   = null;
    private DataOutputStream out     = null;
    // constructor to put ip address and port

    String first = "First Name:";
    String last = "Last Name:";
    String birthdate = "Birthdate (DD.MM.YYYY):";
    String height = "Height:";
    String weight = "Weight:";
    // string to read message from input
    String line = "";

    public void readInput (String variable, String line) {
        try
        {
            System.out.println(variable);
            input  = new DataInputStream(System.in); // takes input from terminal
            out    = new DataOutputStream(socket.getOutputStream()); // sends output to the socket

        }
        catch(UnknownHostException u)
        {
            System.out.println(u);
        }
        catch(IOException i)
        {
            System.out.println(i);
        }

        // keep reading until "Over" is input
       try
       {
           line = input.readLine();
           out.writeUTF(line);
       }
       catch(IOException i)
       {
           System.out.println(i);
       }

    }
    public Client(String address, int port)
    {
        // establish a connection

        try
        {
            socket = new Socket(address, port);
            System.out.println("Connected");

            readInput (first,line);
            readInput (last,line);
            readInput (birthdate,line);
            readInput (height,line);
            readInput (weight,line);


        // close the connection
        
            input.close();
            out.close();
            socket.close();
        }
        catch(IOException i)
        {
            System.out.println(i);
        }
    }

    public static void main(String args[])
    {
        Client client = new Client ("10.201.72.117", 5000);
    }
}